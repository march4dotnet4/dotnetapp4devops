Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction'] = 'Stop'

& dotnet new tool-manifest
& dotnet tool install --global dotnet-sonarscanner
#& nuget restore -NoCache  # restore Nuget dependencies
if ([string]::IsNullOrEmpty("$env:CI_MERGE_REQUEST_IID"))
{
    & echo "Running Branch analysis"
    & dotnet sonarscanner begin `
          /o:"march4dotnet4" `
          /k:"march4dotnet4_dotnetapp4devops" `
          /d:sonar.login="66e2a6d2d4d7d8a0e32dafba4607ef901d88ac2f" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.branch.name="$env:CI_COMMIT_BRANCH"
}
else
{
    & echo "Running SQ Merge Request analysis"
    & dotnet sonarscanner begin `
          /o:"march4dotnet4" `
          /k:"march4dotnet4_dotnetapp4devops" `
          /d:sonar.login="66e2a6d2d4d7d8a0e32dafba4607ef901d88ac2f" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.pullrequest.key="$env:CI_MERGE_REQUEST_IID" `
          /d:sonar.pullrequest.branch="$env:CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" `
          /d:sonar.pullrequest.base="$env:CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
}

& "$env:MSBUILD_PATH" `
    .\dotnetapp4devops\dotnetapp4devops\dotnetapp4devops.csproj # build the project
& dotnet sonarscanner end `
    /d:sonar.login="66e2a6d2d4d7d8a0e32dafba4607ef901d88ac2f"
